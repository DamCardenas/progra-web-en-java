/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enviarCorreo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dam
 */
@WebService(serviceName = "correo")
public class correo {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "enviarCorreo")
    public String enviarCorreo(@WebParam(name = "destinatario")String destinatario,@WebParam(name = "asunto") String asunto,@WebParam(name = "mensaje") String mensaje) throws MalformedURLException, ProtocolException, IOException 
    {
        
        String x =mensaje.replaceAll(" ", "+");
        x=x.replaceAll("\r\n", "%0D%0A");
        URL obj = new URL("http://javaweb.damcardenas.xyz/index.php?destinatario="+destinatario+"&asunto="+asunto+"&mensaje="+x);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();
        //System.out.println("GET Response Code :: " + responseCode);
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                }
                in.close();
                  
                // print result
                System.out.println(response.toString());
                return response.toString();
        } else {
                return "{\"estatus\":0,\"mensaje\":\"Conexion con el servidor fallo :c\"}";
        }
        
    }
}
